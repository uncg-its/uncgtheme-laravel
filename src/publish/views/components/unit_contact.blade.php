<dl title="{{ config('uncgtheme.dept_name') ?: "" }} Contact Information">
    <dt>{{ config('uncgtheme.dept_name') ?: "The University of North Carolina at Greensboro" }}</dt>

    {!! config('uncgtheme.dept_info.address') ? "<dd>" . config('uncgtheme.dept_info.address') . "</dd>" : "" !!}
    {!! config('uncgtheme.dept_info.address2') ? "<dd>" . config('uncgtheme.dept_info.address2') . "</dd>" : "" !!}
    {!! config('uncgtheme.dept_info.phone') ? "<dd>" . config('uncgtheme.dept_info.phone') . "</dd>" : "" !!}
    {!! config('uncgtheme.dept_info.fax') ? "<dd>" . config('uncgtheme.dept_info.fax') . "</dd>" : "" !!}
    {!! config('uncgtheme.dept_info.email') ? "<dd>" . config('uncgtheme.dept_info.email') . "</dd>" : "" !!}

    <dt><strong>The University of North Carolina at Greensboro</strong></dt>
    <dd>Greensboro, NC 27402-6170</dd>

</dl>