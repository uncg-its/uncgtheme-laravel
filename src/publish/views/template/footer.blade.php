</div>

{{--add unit menu to page--}}
@if(config('uncgtheme.unit_menu'))
    @if(config('uncgtheme.page_sidebar')))
    <div class="sidebar col-sm-3 col-sm-pull-6">
        <div class="sidebar-padder">
            @else
                <div class="sidebar col-sm-3 col-sm-pull-9">
                    <div class="sidebar-padder">
                        @endif

                        @include(config('uncgtheme.unit_menu'))

                    </div>
                </div>
            @endif

            {{--add right sidebar to page--}}
            @if(config('uncgtheme.page_sidebar'))
                <div class="sidebar-right col-sm-3">
                    <div class="sidebar-padder">
                        @include(config('uncgtheme.page_sidebar'))
                    </div>
                </div>';
            @endif

        </div>
    </div>
    </div>
    </article>
    </div>
    </div>


    </div>
    <div id="footer" class="site-footer" role="contentinfo">
        <div id="footer-content" class="row">
            <div id="footer-left" class="site-footer-inner-left col-xs-12 col-sm-4 col-md-6">
                @if(config('uncgtheme.views.unit_contact'))
                    @include(config('uncgtheme.views.unit_contact'))
                @else
                    <dl title="Contact Information">
                        <dt><strong>UNC Greensboro</strong></dt>
                        <dd>Greensboro, NC 27402-6170</dd>
                    </dl>
                @endif


                @if(config('uncgtheme.admin_info'))
                    @include(config('uncgtheme.admin_info'))
                @endif

            </div>
            <div id="footer-right" class="site-footer-inner-right col hidden-xs">
                <?php require_once(public_path() . '/vendor/uncg/wrapper/rincuncg/footer.htm');?>
            </div>
        </div>
    </div>
    <div style="clear:both;display:block;height:0">&nbsp;</div>
    </div>
    <div id="shadow-bottom">&nbsp;</div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"
        integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP"
        crossorigin="anonymous"></script>

    <script type="text/javascript" src="{{ asset('/vendor/uncg/wrapper/rincuncg/js/common.min.js') }}"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ asset('/vendor/uncg/wrapper/rincuncg/js/lt-ie-9.min.js') }}"></script>
    <![endif]-->

    <script src="{{ asset('js/ccps-core.js') }}"></script>

    @if(config('uncgtheme.custom_js'))
        <script src="{{ mix('uncg/js/' . config('uncgtheme.custom_js')) }}"></script>
        @endif

        @yield('scripts')

        </body>
        </html>
