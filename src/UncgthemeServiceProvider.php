<?php

namespace Uncgits\Uncgtheme;

use Illuminate\Support\ServiceProvider;

class UncgthemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register artisan commands, other CLI stuff
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Uncgits\Uncgtheme\Command\PublishAssets::class,
                \Uncgits\Uncgtheme\Command\SetupSymlinks::class,
                \Uncgits\Uncgtheme\Command\RemoveAssets::class,
                \Uncgits\Uncgtheme\Command\Install::class,
            ]);

            // publish wrapper assets to public/vendor
            $this->publishes([
                __DIR__ . '/publish/wrapper' => public_path('vendor/uncg/wrapper'),
            ], 'wrapper');

            // get views folder
            $viewPath = Helper::getViewsFolder();

            // publish assets and default view
            $this->publishes([
                __DIR__ . '/publish/assets' => $viewPath . '/uncg/assets',
            ], 'assets');

            // publish assets and default view
            $this->publishes([
                __DIR__ . '/publish/views'  => $viewPath . '/uncg'
            ], 'views');

            // publish config
            $this->publishes([
                __DIR__ . '/publish/config' => base_path('config'),
            ], 'config');
        }

        // views
        $this->loadViewsFrom(__DIR__ . '/publish/views/', 'uncgtheme');
    }



    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
