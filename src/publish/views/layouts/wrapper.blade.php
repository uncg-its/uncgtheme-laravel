<?php

/* PAGE_TITLE is displayed as part of the page title in the browser tab. */
//define('PAGE_TITLE', '');

?>

@include('uncgtheme::template.header', [
    'pageTitle' => $pageTitle,
    'showUnitHead' => $showUnitHead ?? false,
    'thin' => false
])

<!-- PAGE CONTENT BEGIN -->
<div class="container-fluid">
    @include('components.flash')
    @yield('content')
</div>
<!-- PAGE CONTENT END -->

@include('uncgtheme::template.footer')