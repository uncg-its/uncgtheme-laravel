<?php

namespace Uncgits\Uncgtheme\Command;

use Illuminate\Console\Command;
use Uncgits\Uncgtheme\Helper;

class RemoveAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uncgtheme:remove-assets
                            {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes assets for the UNCG Theme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function removeAssets($assetSet) {
        $this->info('Removing assets for UNCG Theme...');

        foreach($assetSet as $asset) {
            if (is_dir($asset)) {
                \File::deleteDirectory($asset);
            } else {
                \File::delete($asset);
            }

            $this->info('Removed ' . $asset);
        }

        $this->info('Assets removed.');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get views folder
        $viewPath = Helper::getViewsFolder();

        // define asset sets
        $assets = [
            'all' => [
                public_path('vendor/uncg/'),
                base_path('config/uncgtheme.php'),
                $viewPath . '/uncg/',
            ],
            'public' => [
                public_path('vendor/uncg/'),
            ]
        ];

        $removeAll = $this->option('all');

        $assetsToRemove = $removeAll ? 'all' : 'public';

        $message = $removeAll ? 'This will remove ALL installed assets for the theme, including configuration files and customizations.' : 'This will remove only the public folder assets for the theme. If you wish to remove all assets, including configuration files, use the --all flag.';

        if ($this->confirm($message . PHP_EOL . 'Are you sure you wish to continue?')) {
            return $this->removeAssets($assets[$assetsToRemove]);
        }

        return false;

    }
}
