<?php

namespace Uncgits\Uncgtheme\Command;

use Illuminate\Console\Command;

class PublishAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uncgtheme:publish-assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes assets for the UNCG Theme (first-time publish)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // publish wrapper assets to public/vendor
        $this->info('Publishing wrapper assets for UNCG Theme...');
        $this->call('vendor:publish', [
            '--provider' => 'Uncgits\\Uncgtheme\\UncgthemeServiceProvider',
            '--tag' => 'wrapper'
        ]);

        // publish public js / css assets
        $this->info('Publishing assets for UNCG Theme...');
        $this->call('vendor:publish', [
            '--provider' => 'Uncgits\\Uncgtheme\\UncgthemeServiceProvider',
            '--tag' => 'assets'
        ]);

        // publish config file
        $this->info('Publishing config for UNCG Theme...');
        $this->call('vendor:publish', [
            '--provider' => 'Uncgits\\Uncgtheme\\UncgthemeServiceProvider',
            '--tag' => 'config'
        ]);

        $this->info('Publishing done.');

        return true;

    }
}
