# UNCG Theme for Laravel (CCPS Core)

author: Matt Libera (matt.libera@uncg.edu)

## Introduction

This package makes the UNCG standard wrapper (based on Bootstrap) available to be used in Laravel. This package is designed to be used internally at UNCG for web application development.

Based on the UNCG responsive wrapper, August 2018 revision: [https://its.uncg.edu/web-services/templates/](https://its.uncg.edu/web-services/templates/)

## Version History

### 1.8

- Changes for PSR-4

### 1.7.3

- Attempts to fix a lingering reference to Trade Gothic font

### 1.7.2

- Override `.fade` opacity setting from the UNCG Assets back to `initial` so that modals in the new Bootstrap version work again..

### 1.7.1

- Update Bootstrap and FontAwesome assets (and dependencies)
- Sniff application environment setting before applying Google Analytics

### 1.7

Replaces Trade Gothic font in `rincuncg/css/common.min.css` with new Pluto Sans

### 1.6 update

As of version 1.6 the package now works for both the "official" TLD method of installing Laravel, and also the symlink method of installing Laravel. Previously, this package would function in both environments, but if you created the app one way and tried to deploy it another (for instance, developed at http://myapp.local and then tried to deploy to https://myserver.uncg.edu/myapp), the symlinked assets would break.

**If you are upgrading from a previous version and need this functionality, recreate the symlink by doing the following:**
1. `cd` to your app's root
2. `rm public/vendor/uncg/wrapper/rincuncg`
3. `php artisan uncgtheme:setup-symlinks`
4. Commit your changes and push to repo

## Installation

#### 1. Require the package in `composer.json`

Run `composer require uncgits/uncgtheme-laravel` from the command line

#### 2. Register service provider (Laravel 5.4 and below only)

The Service Provider will be auto-discovered if using Laravel 5.5+, but if using 5.4 or below, add the following to your providers list in `config/app.php`:

```
Uncgits\Uncgtheme\UncgthemeServiceProvider::class,
```

#### 3. Complete theme installation

After installing the package, run the following `artisan` command:

- `php artisan uncgtheme:install` - this will run `php artisan uncgtheme:publish-assets` and `php artisan uncgtheme:setup-symlinks` - you could also run these individually.

Read below if you want to know more about what these commands do, specifically.

#### 4. Configure the theme

Inside of `config/uncgtheme.php`, set up the variables to be used for the theme. You are provided with Blade templates and example optional components as part of the theme installation - many need to be activated in the `config/uncgtheme.php` file before they will be used, though. Read the comments in that file for more information.

## Using the theme

First ensure that the theme configuration (Step 4 above) is done. You will want to make sure your nav menu Blade file name is inserted into `views.nav` if different from default CCPS Core.

### Deploying on Single pages

If you want to use the wrapper in place of CCPS Core's default bootstrap wrapper, simply replace the following:

```
@extends('layouts.wrapper', [
    'pageTitle' => 'Whatever'
])
```

with:

```
@extends('uncgtheme::layouts.wrapper', [
    'pageTitle' => 'Whatever'
])
```

### Using the Unit Head

The Unit Head area injects information defined in the `config/uncgtheme.php` file into the theme as a header. However, this can be cumbersome to do on all pages, so the theme accepts a `showUnitHead` variable when you extend it via Blade. So, if you want to show the unit head, you would use:

```
@extends('uncgtheme::layouts.wrapper', [
    'pageTitle' => 'Whatever',
    'showUnitHead' => true
])
```

### Deploying on Entire site

If you want to use this theme on your entire site, rearrange your `layouts.wrapper` file to call the UNCG Theme wrapper itself (passing in both `pageTitle` and `showUnitHead`, recommend using the null coalesce operator on the latter), as follows:

```
// /resources/views/layouts/wrapper.blade.php

@extends('uncgtheme::layouts.wrapper', [
    'pageTitle' => $pageTitle
    'showUnitHead' => $showUnitHead ?? false
])

// whatever.blade.php - using unit head

@extends('layouts.wrapper', [
    'pageTitle' => 'Whatever
    'showUnitHead' => true
])

```

If you do this, best-practice would be to rename your original `layouts.wrapper` view to something like `wrapper-bs4.blade.php` so that you can use it as needed by calling `@extends('layouts.wrapper-bs4')`

### Thin header version

The August 2018 version of the UNCG Responsive Website Assets contianed extra classing to be able to utilize a thin header instead of the full University header. This theme package supports the thin header by simply including a different view:

```
@extends('uncgtheme::layouts.wrapper-thin', [
    'pageTitle' => 'Whatever'
])
```

This can be done as above for either single-page or entire-site deployments.

## Publishing views

If you want to use your own views instead of the package views, call `vendor:publish --provider=Uncgits\Uncgtheme\UncgthemeServiceProvider --tag=views` and the views will be published to the `resources/views/uncg/` folder.

## Package updates

As of now all updates are contained within the package's `vendor` folder and the symlinked `rincuncg` assets. No updates should be necessary to published content.

## Removing the theme

From the command line, you may remove the `public/vendor/uncg` folder by running the `php artisan uncgtheme:remove-assets`. This will leave your config file and Blade files.

Optionally, if you wish to remove ALL files, including the config and Blade files, you can run `php artisan uncgtheme:remove-assets --all`. Note that this will (obviously) blow away any changes you've made to the config file or customizations to the Blade template.

## Artisan commands in detail

The artisan commands in the package function as follows:

- `php artisan uncgtheme:publish-assets` - this will put the files from the various locations in the package into your project: essential wrapper files are placed into `public/vendor/uncg/wrapper`, placeholder JS and SASS files for overrides are placed into `uncg/assets` and `uncg/views` in your `resources/views` folder, and the `uncgtheme.php` config file is placed into the `config` folder.

- `php artisan uncgtheme:setup-symlinks` - this completes the setup of the UNCG responsive wrapper assets (linking from your `public` folder into the `vendor` folder). A `rincuncg` symlink will be created in `public/vendor/uncg/wrapper` which links to the `rincuncg` folder inside of the composer assets. This helps Laravel to reference certain components of the wrapper even if they are not housed in the `public` folder. (These are also the assets that will be updated as the UNCG wrapper is updated, and so they need to be housed in `vendor` as part of the package).

- `php artisan uncgtheme:remove-assets` - as described above, this removes the published wrapper assets from `public/vendor/uncg` with option to also remove the config file and views/assets from the theme folder.

## Known issues

No currently known issues at time of release.

## Questions

Contact me with any issues. Work is ongoing!
