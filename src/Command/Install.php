<?php

namespace Uncgits\Uncgtheme\Command;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uncgtheme:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs UNCG Theme (copies assets and creates symlinks) - calls the publish-assets and setup-symlinks artisan commands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Installing UNCG Theme...');

        $this->call('uncgtheme:publish-assets');
        $this->call('uncgtheme:setup-symlinks');

        $this->info('UNCG Theme installed successfully.');

        return true;

    }
}
