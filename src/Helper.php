<?php

namespace Uncgits\Uncgtheme;

class Helper {
    public static function getViewsFolder() {
        // determine install location for assets and default views
        $themes_config = config('themes');
        if (is_null($themes_config)) {
            // use Laravel default
            $viewPath = resource_path('views');
        } else {
            // igaster Theme plugin
            $viewPath = config('themes')['themes_path'];
        }

        return $viewPath;
    }
}