<?php

namespace Uncgits\Uncgtheme\Command;

use Illuminate\Console\Command;

class SetupSymlinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uncgtheme:setup-symlinks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets up all symlinks for the UNCG Theme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Creating symlinks for UNCG Theme...');
        // $target = base_path("vendor/uncgits/uncgtheme-laravel/src/rincuncg");
        $target = "../../../../vendor/uncgits/uncgtheme-laravel/src/rincuncg";
        // $link = public_path() . "/vendor/uncg/wrapper/rincuncg";
        $link = "public/vendor/uncg/wrapper/rincuncg";

        $this->info('Using target ' . $target);
        $this->info('Using link ' . $link);

        if (symlink($target, $link)) {
            $this->info('Symlinked rincuncg assets.');
        } else {
            $this->error('Error! Unable to symlink rincuncg assets.');
        }

        return true;
    }
}
