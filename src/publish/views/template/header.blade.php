<!DOCTYPE html>
<!-- UNCG WEB3 WRAPPER::HEADER::PHP VERSION 3.0.0::9/1/2014 -->
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('uncgtheme.unit_name') ?: "UNCG" }} | {{ $pageTitle }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">

    <link rel='stylesheet' id='uncg-libraries'
          href='{{ asset('/vendor/uncg/wrapper/rincuncg/css/lib.min.css?ver=1.0.0') }}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='uncg-print-css'
          href='{{ asset('/vendor/uncg/wrapper/rincuncg/css/print.min.css?ver=1.0.0') }}'
          type='text/css' media='print'/>
    <link rel='stylesheet' id='fastfonts-css'
          href='//fast.fonts.com/cssapi/7860d9f9-615c-442e-ab9b-bd4fe7794024.css?ver=3.9.2' type='text/css'
          media='all'/>
    <link href="{{ asset('/vendor/uncg/wrapper/rincuncg/css/common.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/vendor/uncg/wrapper/css/wrapper-extras.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('/vendor/uncg/wrapper/rincuncg/css/horizontal-nav.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/vendor/uncg/wrapper/rincuncg/css/print.min.css') }}" rel="stylesheet"
          type="text/css" media="print"/>

    @if(config('uncgtheme.page_header'))
        @include(config('uncgtheme.page_header'))
    @endif

    @if(config('app.env') == 'production')
    <script type="text/javascript">/* Google Analytics */
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34361385-1']);
        _gaq.push(['_setDomainName', '.uncg.edu']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
    @endif

    <!--[if lt IE 9]>
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('/vendor/uncg/wrapper/rincuncg/css/lt-ie-9.min.css') }}"/>
    <![endif]-->

    <link href="{{ asset('css/ccps-core.css') }}" rel="stylesheet">

    @if(config('uncgtheme.custom_css'))
        <link href="{{ mix('uncg/css/' . config('uncgtheme.custom_css')) }}" rel="stylesheet" type="text/css"/>
    @endif

<!-- Page head items -->
    @yield('head')

    <style>
        b.caret {
            display: none;
        }

        .main-content .navbar {
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
        }

        .main-content .navbar:after,
        .main-content .navbar:before {
            content: none;
            display: initial;
        }

        #unit-head {
            padding: 10px 0 14px 0;
            margin-bottom: 14px;
        }

        .main-content .navbar-text {
            margin-top: 0;
        }

        h3, h4, h5, h6 {
            margin: 0 0 16px;
        }

        .navbar-brand {
            padding-right: 0;
        }

        .breadcrumb {
            margin-bottom: 0;
        }

        .modal.fade .modal-dialog {
            -webkit-transform: translate(0, 0%);
            -ms-transform: translate(0, 0%);
            -o-transform: translate(0, 0%);
            transform: translate(0, 0%);
        }

        .fade.show {
            background-color: rgba(0, 0, 0, 0.5);
        }

        .fade {
            opacity: initial;
        }

        .footer-logo {
            left: 44px;
        }

        #navMenu1::after {
            /* Not sure about this... */
            display: none;
        }

        @media (min-width: 576px) {
            #navMenu1::after {
                /* Not sure about this... */
                display: none;
            }

            .container {
                max-width: 767px;
            }

            .thin #navMenu1 {
                display: block !important;
            }

            .thin .unit-nav.show {
                display: block !important;
            }
        }

        @media (min-width: 768px) {
            #header-left {
                float: none;
                position: initial;
            }

            #header-right {
                float: none;
            }

            .main-content .navbar-collapse.collapse {
                display: none !important;
            }

            .main-content .navbar-collapse.collapse.show {
                display: block !important;
            }

            .main-content .navbar-text {
                margin-left: 0;
                margin-right: 0;
                float: none;
            }

            .main-content .navbar-nav {
                float: none;
            }

            #tools-nav ul {
                float: right;
            }

            .container {
                max-width: 750px;
            }

            #header-extras .dropdown-toggle {
                text-align: right;
                position: relative;
                top: 67px;
            }

            .unit-nav {
                position: absolute;
                top: 116px;
            }

            .thin .unit-nav {
                top: 67px;
            }

        }

        @media (min-width: 992px) {
            #nav-container, .shadow-box {
                max-width: 970px;
            }

            .main-content .navbar-collapse.collapse,
            .main-content .navbar-collapse.show {
                display: flex !important
            }
        }

        .thin .main-nav.hidden-md,
        .thin .unit-nav.hidden-md.show {
            /*display: block !important;*/
        }
    </style>
</head>
@if($thin)
    <body class="thin" ontouchstart=""> @else
        <body ontouchstart=""> @endif

        <ul id="uncgAccessNav">
            <li><a href="#startcontent">Skip to Main Content</a></li>
        </ul>

        <?php require_once(public_path() . '/vendor/uncg/wrapper/rincuncg/heading.php'); ?>

        <div class="shadow-box">
            <div class=""><a name="startcontent"></a>

                <div class="main-content ">

                    @if(config('uncgtheme.unit_goldbar'))
                        {{-- Manual menu --}}
                        @include(config('uncgtheme.unit_goldbar'))
                    @else
                        {{-- Built in menu (using Lavary package) --}}
                        @include(config('uncgtheme.views.nav'))
                    @endif

                    <div>
                        <article>
                            <div class="container-fluid">


                                {{-- display header block if defined --}}
                                @if($showUnitHead && (config('uncgtheme.unit_sub_title') || config('uncgtheme.dept_name')))
                                    <div class="row">
                                        <div class="col">
                                            <div id="unit-head" class="row">
                                                <div class="col">
                                                    {!! config('uncgtheme.dept_name') ? '<h1 class="site-title">'. config('uncgtheme.dept_name') .'</h1>' : "" !!}
                                                    {!! config('uncgtheme.unit_sub_title') ? '<h2 class="site-title">'. config('uncgtheme.unit_sub_title') .'</h2>' : "" !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif


                                <div id="content-wrapper">
                                    {{--determine which of the 4 layouts is being implemented--}}
                                    @if(config('uncgtheme.unit_menu') && config('uncgtheme.page_sidebar'))
                                        <div class="row for-sidebar">
                                            <div id="content" class="main-content-inner col-xs-6 col-xs-push-3">
                                                @elseif(config('uncgtheme.unit_menu'))
                                                    <div class="row">
                                                        <div id="content"
                                                             class="main-content-inner  col-xs-9 col-xs-push-3">
                                                            @elseif(config('uncgtheme.page_sidebar'))
                                                                <div class="row for-sidebar">
                                                                    <div id="content"
                                                                         class="main-content-inner col-xs-9">
                                                                        @else
                                                                            <div class="row">
                                                                                <div id="content"
                                                                                     class="main-content-inner col-xs-12">
@endif
